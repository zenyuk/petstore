package main

// GetPet - returns a pet
func (p *Pet) GetPet(ID int, result *Pet) error {
	println("get pet")
	var ok bool
	*result, ok = p.AllPets[ID]
	if !ok {
		(*result).ID = -1
	}
	return nil
}

// AddPet - adds a new pet
func (p *Pet) AddPet(pet Pet, result *Pet) error {
	p.AllPets[pet.ID] = pet
	*result = pet
	return nil
}
