package main

import (
	"fmt"
	"log"
	"net"
	"net/http"
	"net/rpc"
)

func main() {
	initialPet := Pet{
		ID:   3,
		Name: "initial",
	}
	allPets := map[int]Pet{
		3: initialPet,
	}
	pet := Pet{
		AllPets: allPets,
	}
	rpc.Register(&pet)
	rpc.HandleHTTP()
	lis, err := net.Listen("tcp", ":1234")
	if err != nil {
		log.Fatal("Error to start listening Pet service on port 1234 ", err)
	}
	fmt.Println("Pet service started on port 1234")
	go http.Serve(lis, nil)
	select {}
}
