package main

// Pet - represents a home pet
type Pet struct {
	ID        int      `json:"id"`
	Category  Category `json:"category"`
	Name      string   `json:"name"`
	PhotoUrls []string `json:"photoUrls"`
	Tags      []Tag    `json:"tags"`
	Status    string   `json:"status"`
	AllPets   map[int]Pet
}

// Category - pet's category
type Category struct {
	ID   int    `json:"id"`
	Name string `json:"name"`
}

// Tag - pet's tag
type Tag struct {
	ID   int    `json:"id"`
	Name string `json:"name"`
}
