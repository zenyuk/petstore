package main

import (
	"fmt"
	"net/http"
)

func main() {
	m := http.NewServeMux()
	m.Handle("/pet", http.HandlerFunc(AddPet))
	m.Handle("/pet/", http.HandlerFunc(GetPet))
	fmt.Println("WebApi service started")
	http.ListenAndServe(":8081", m)
}
