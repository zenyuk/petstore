package main

import (
	"encoding/json"
	"fmt"
	"net/http"
	"net/rpc"
	"path"
	"strconv"
)

// AddPet - adds a new pet to the store
func AddPet(w http.ResponseWriter, r *http.Request) {
	var p Pet
	dec := json.NewDecoder(r.Body)
	err := dec.Decode(&p)
	if err != nil {
		w.Write([]byte("Pet could not be parsed"))
	}

	client, err := rpc.DialHTTP("tcp", "localhost:1234")
	if err != nil {
		fmt.Fprint(w, "Error connecting to Pet service ")
	}

	var result Pet
	client.Call("Pet.AddPet", p, &result)
	enc := json.NewEncoder(w)
	enc.Encode(result)
}

// GetPet - returns a new pet from the store
func GetPet(w http.ResponseWriter, r *http.Request) {
	petID, err := strconv.Atoi(path.Base(r.URL.Path))
	if err != nil {
		fmt.Fprint(w, "Invalid Pet id")
	}

	client, err := rpc.DialHTTP("tcp", "localhost:1234")
	if err != nil {
		fmt.Fprint(w, "Error connecting to Pet service ")
	}

	var p Pet
	client.Call("Pet.GetPet", petID, &p)
	println(p.ID)
	if p.ID < 0 {
		fmt.Fprint(w, "Invalid Pet id")
		return
	}
	enc := json.NewEncoder(w)
	enc.Encode(p)
}
