Pet store test task

** api
https://petstore.swagger.io/#/

** how to run
build & install & run:
- /petstore/pet/main.go
- /petstore/webapi/main.go

** hit endpoints:
- http://localhost:8081/pet/3 (http get)
- http://localhost:8081/pet (http post with json body)